# !/usr/bin/env python

import random
import hashlib
import hmac
from string import letters

from google.appengine.ext import db
from google.appengine.api import memcache

# Import common functions and variables
from common.common import *

"""
 User class and helper functions
"""

def make_salt(length = 5):
    """ Return a random string of length `length`; useful for salts. """
    return ''.join(random.choice(letters) for x in xrange(length))

def make_pw_hash(name, pw, salt = None):
    """ Return a tuple containing the salt and the password hash. """
    if not salt:
        salt = make_salt()
    h = hashlib.sha256(name + pw + salt).hexdigest()
    return '%s,%s' % (salt, h)

def valid_pw(name, password, h):
    """ Returns true if the password hash `h` contains `password`. """
    salt = h.split(',')[0]
    return h == make_pw_hash(name, password, salt)

def users_key(group = 'default'):
    """ Set the db key for the User class. """
    return db.Key.from_path('users', group)


class User(db.Model):
    """ Class to represent users of the wiki. """
    name = db.StringProperty(required = True)
    pw_hash = db.StringProperty(required = True)
    email = db.StringProperty()

    @classmethod
    def by_id(cls, uid):
        """ Rerturns the User object identified by `uid`. """
        return User.get_by_id(uid, parent = users_key())

    @classmethod
    def by_name(cls, name):
        """ Return the User object identified by `name`. """
        u = User.all().filter('name =', name).get()
        return u

    @classmethod
    def register(cls, name, pw, email = None):
        """ Create a user given `name`, `pw`, and `email`. """
        pw_hash = make_pw_hash(name, pw)
        return User(parent = users_key(),
                    name = name,
                    pw_hash = pw_hash,
                    email = email)

    @classmethod
    def login(cls, name, pw):
        """ Log in a user. """
        u = cls.by_name(name)
        if u and valid_pw(name, pw, u.pw_hash):
            return u

"""
 Page class and helper functions
"""

def cap(s, l):
    """ Limit the length of a string s to l characters
    from http://stackoverflow.com/questions/11602386/python-function-for-capping-a-string-to-a-maximum-length """
    return s if len(s)<=l else s[0:l-3]+'...'

def page_key(group = 'default'):
    """ Set the db key for the page class. """
    return db.Key.from_path('page', group)

class Page(db.Model):
    """ Class used to represent a page in the wiki. """
    title = db.StringProperty(required = True)
    content = db.TextProperty(required = True)
    revision = db.StringProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True )

    @classmethod
    def write_page(cls, title, content, revision):
        """ Writes out wiki page to the DB and memcache. """
        p = Page(parent = page_key(), title = title, content = content, revision = revision)
        p.put()
        memcache.set(title,p)
    
    @classmethod
    def by_title(cls, title):
        """ Returns the most recent page given its title (not used). """
        p = Page.all().filter('title =', title).get()
        return p
    
    @classmethod
    def most_recent(cls, title):
        """ Returns the most recent revision of a page. """
        key = 'post_key'
        p = memcache.get(title)
        if p is None:
            p = Page.all().filter('title =', title).order('-created').get()
            memcache.set(title,p)
        return p

    @classmethod
    def by_revision(cls, title, revision):
        """ Returns the revision of a given wiki page. """
        p = Page.all().filter('title =', title).filter('revision =', revision).get()
        return p
   
    @classmethod
    def page_history(cls, title, number=20):
        """ Returns number most recent revisions of a wiki page. """
        p = Page.all().filter('title =', title).order('-created')
        if p.count() != 0:
            return p.run(limit=number)
    
    def render_abridged_page(self):
        """ Render an abridged version of a page to use in the page history view. """
        self._render_text = cap(self.content, 100)
        return render_str("history_instance.html", p = self)
    
