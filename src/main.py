#!/usr/bin/env python

import fix_path

# Importing the controllers that will handle
# the generation of the pages:
from controllers import wiki

# Importing some of Google's AppEngine modules:
import webapp2
from google.appengine.ext.webapp import util

def main():
    """ Main method that handles the defined routes.
    If route is requested that is not defined here then a 404 will be returned. """
    PAGE_RE = r'(/(?:[a-zA-Z0-9_-]+/?)*)'
    application = webapp2.WSGIApplication([
        ('/signup', wiki.Register),
        ('/welcome', wiki.Welcome),
        ('/login', wiki.Login),
        ('/logout', wiki.Logout),
        ('/_edit' + PAGE_RE, wiki.EditPage),
        ('/_history' + PAGE_RE, wiki.HistoryPage),
        (PAGE_RE, wiki.WikiPage),
    ],debug=True)
    util.run_wsgi_app(application)

if __name__ == '__main__':
    main()
