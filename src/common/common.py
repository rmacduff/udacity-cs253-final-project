#!/usr/bin/env python


"""
 Set up console logging

 Add logging anywhere by including:
 logging.debug("your message")
"""

import logging

# From: https://docs.python.org/2/howto/logging-cookbook.html
# define a Handler which writes DEBUG messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)


"""
 Jinja templating helper code
"""

from bleach import clean
from markupsafe import Markup
import jinja2
import os

ALLOWED_TAGS = ['a', 'img', 'ol', 'ul', 'li', 'hr', 'br', 'h1', 'h2', 'h3', 'h4', 'em', 'b' ]

def escape_html(text, **kw):
    """ Return an escaped version of text.  
    Escapes all tags except what is specified by ALLOWED_TAGS."""
    return Markup(clean(text, tags=ALLOWED_TAGS, **kw))

def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)

template_dir = os.path.join(os.path.dirname(__file__), '../views/')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir),
                                               autoescape = True)
jinja_env.filters['escape_html'] = escape_html
