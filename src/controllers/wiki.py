#!/usr/bin/env python

import webapp2
import re
import time

# Include the models:
from models.models import *

# Include the common functions and variables
from common.common import *

# Secret used in hashing hashing password
secret = 'XXXXXXXXXX'

"""
 Helper functions
"""

def make_secure_val(val):
    """ Return a string containing val and its encrypted version separated by '|' - 
    useful for setting secure values for cookies. """
    return '%s|%s' % (val, hmac.new(secret, val).hexdigest())

def check_secure_val(secure_val):
    """ Return true if secure_val is an actual valid value, encrypted value pair. """
    val = secure_val.split('|')[0]
    if secure_val == make_secure_val(val):
        return val


class WikiHandler(webapp2.RequestHandler):
    """ Base wiki handler class. """
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **kw):
        """ Return the rendered jinja template using the given kw paramaters. """
        kw['user'] = self.user
        t = jinja_env.get_template(template)
        return t.render(kw)

    def render(self, template, **kw):
        """ Write out the template using the kw paramaters."""
        self.write(self.render_str(template, **kw))

    def render_login(self, *a, **kw):
        """ Render the login page. """
        self.render('login.html', *a, **kw)

    def render_signup(self, *a, **kw):
        """ Render the signup page. """
        self.render('signup.html', *a, **kw)

    def render_welcome(self, *a, **kw):
        """ Render the welcome page. """
        self.render('welcome.html', *a, **kw)

    def render_view(self, *a, **kw):
        """ Render the view page. """
        self.render('view.html', *a, **kw)

    def render_edit(self, *a, **kw):
        """ Render the edit page. """
        self.render('edit.html', *a, **kw)
        
    def render_history(self, *a, **kw):
        """ Render the history page. """
        self.render('history.html', *a, **kw)

    def set_secure_cookie(self, name, val):
        """ Set a cookie for val that also has its encrypted version. """ 
        cookie_val = make_secure_val(val)
        self.response.headers.add_header(
            'Set-Cookie',
            '%s=%s; Path=/' % (name, cookie_val))

    def read_secure_cookie(self, name):
        """ Return true if login cookie is valid. """
        cookie_val = self.request.cookies.get(name)
        return cookie_val and check_secure_val(cookie_val)

    def login(self, user):
        """ Set login cookie after logging in. """
        self.set_secure_cookie('user_id', str(user.key().id()))

    def logout(self):
        """ Unset login cookies when logging out. """
        self.response.headers.add_header('Set-Cookie', 'user_id=; Path=/')

    def make_path(self):
        """ Return a path based on the current request parameters."""
        page_edit = self.request.get('edit')
        page_title = self.request.get('redirect_page')
        path = ''
        if page_title:
            if page_edit == 'true':
                path = '/_edit/%s' % (page_title)
            else:
                path = '/%s' % (page_title)
        else:
            path = '/welcome'
        return path

    def make_query_string(self):
        """ Return a query string with. """
        revision = self.request.get('revision')
        query_str = ''
        if revision:
            query_str = '?revision=%s' % (revision)
        return query_str

    def redirect_from_login(self):
        """ Redirect to the appropriate page after logging in. """
        url = self.make_path() + self.make_query_string()
        self.redirect(url)

    def initialize(self, *a, **kw):
        """ Set the user for each request, if valid. """
        webapp2.RequestHandler.initialize(self, *a, **kw)
        uid = self.read_secure_cookie('user_id')
        self.user = uid and User.by_id(int(uid))


"""
 User registrations, login and logout classes and methods
"""

USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
def valid_username(username):
    return username and USER_RE.match(username)

PASS_RE = re.compile(r"^.{3,20}$")
def valid_password(password):
    return password and PASS_RE.match(password)

EMAIL_RE  = re.compile(r'^[\S]+@[\S]+\.[\S]+$')
def valid_email(email):
    return not email or EMAIL_RE.match(email)


class Signup(WikiHandler):
    """ Class to provide basic functionality for signing up users to the wiki."""
    def get(self):
        self.render_signup()

    def post(self):
        have_error = False
        self.username = self.request.get('username')
        self.password = self.request.get('password')
        self.verify = self.request.get('verify')
        self.email = self.request.get('email')

        params = dict(username = self.username,
                      email = self.email)

        if not valid_username(self.username):
            params['error_username'] = "That's not a valid username."
            have_error = True

        if not valid_password(self.password):
            params['error_password'] = "That wasn't a valid password."
            have_error = True
        elif self.password != self.verify:
            params['error_verify'] = "Your passwords didn't match."
            have_error = True

        if not valid_email(self.email):
            params['error_email'] = "That's not a valid email address."
            have_error = True

        if have_error:
            self.render_signup(**params)
        else:
            self.done()

    def done(self, *a, **kw):
        """ See Register class."""
        raise NotImplementedError


class Register(Signup):
    """ Class invoked when visitors register. """ 
    def done(self):
        u = User.by_name(self.username)
        if u:
            msg = 'That username already exists.'
            self.render_signup(error_username = msg)
        else:
            u = User.register(self.username, self.password, self.email)
            u.put()

            self.login(u)
            self.redirect_from_login()

class Login(WikiHandler):
    """ Class to handle logging in users to the wiki. """
    def get(self):
        self.render_login(query_str = self.request.query_string)

    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')

        u = User.login(username, password)
        if u:
            self.login(u)
            self.redirect_from_login()
        else:
            msg = 'Invalid login'
            self.render_login(error = msg)


class Logout(WikiHandler):
    """ Class to handle logging out users from the wiki. """
    def get(self):
        self.logout()
        # redirect the user to the page they were on before logging out
        self.redirect(self.request.referer)


class  Welcome(WikiHandler):
    """ Class to handle the wiki welcome page. """
    def get(self):
        self.render_welcome()

#
# Page view and edit classes and methods
#

def get_page_title(page_path):
    return page_path.replace('/', '', 1)

class WikiPage(WikiHandler):
    """ Class to handle displaying a wiki page. """
    def get(self, path):
        title = get_page_title(path)
        revision = self.request.get('revision')

        # If page exists render it, otherwise redirect to editpage
        if revision:
            page = Page.by_revision(title, revision)
            if not page:
                # That revision didn't exist so get most recent
                page = Page.most_recent(title)
        else:
            page = Page.most_recent(title)
        
        if page:
            # page exists so render it
            self.render_view(page = page)
        elif len(title) ==  0:
            # no page name/path so just render the welcome page 
            self.render_welcome()
        else:
            # page doesn't exit so create it
            self.redirect("/_edit" + path)

class EditPage(WikiHandler):
    """ Class to handle editing a wiki page. """
    
    def get(self, path):
        title = get_page_title(path)
        revision = self.request.get('revision')

        if revision:
            page = Page.by_revision(title, revision)
            if not page:
                # That revision didn't exist so get most recent
                page = Page.most_recent(title)
        else:
            page = Page.most_recent(title)
       
        # Make sure visitor is logged in
        if self.user:
            if page:
                self.render_edit(page = page, title = title)
            else:
                self.render_edit(title = title)
        else:
            if revision:
                self.redirect("/login?edit=true&redirect_page=%s&revision=%s" % (title, revision))
            else:
                self.redirect("/login?edit=true&redirect_page=%s" % (title))

    def post(self, path):
        content = self.request.get('content')
        title = get_page_title(path)
        most_recent_page = Page.most_recent(title)

        if most_recent_page:
            revision = str(int(most_recent_page.revision) + 1)
        else:
            revision = '1'

        # Make sure visitor is logged in before writing
        if self.user:
            Page.write_page(title, content, revision)
            # need to add an artificial delay before redirecting to allow DB propagation
            time.sleep(0.5) 
            self.redirect(path)
        else:
            # we want to remember what page this user wanted to edit
            if revision:
                self.redirect("/login?edit=true&redirect_page=%s&revision=%s" % (title, revision))
            else:
                self.redirect("/login?edit=true&redirect_page=%s" % (title))
        
 


class HistoryPage(WikiHandler):
    """ Class to handle displaying the history page for a wiki page. """

    def get(self, path):
        title = get_page_title(path)
        pages = Page.page_history(title)

        if pages:
            self.render_history(pages = pages, title = title)
        else:
            # if page does not exist create it
            self.redirect("/_edit" + path)
